import React, { useContext } from "react";
import AppContext from "@context/AppContext";

//Importing components
import HomeItem from "@components/HomeItem";

const HomeList = () => {
  const { state, deleteTask } = useContext(AppContext);

  return (
    <tbody>
      {state.tasks.length > 0 ? (
        state.tasks.map((t) => <HomeItem key={t.id} t={t} d={deleteTask} />)
      ) : (
        <tr>
          <td>Loading...</td>
        </tr>
      )}
    </tbody>
  );
};

export default HomeList;

import React, { useContext, useState } from "react";
import AppContext from "@context/AppContext";

const HomeForm = () => {
  const initialState = { id: 0, title: "", description: "" };
  const { saveTask } = useContext(AppContext);
  const [task, setTask] = useState(initialState);

  //Manejador de cambio de inputs
  const handleChange = (name, value) => setTask({ ...task, [name]: value });

  //Generamos un ID aleatorio
  const handleRandom = () => {
    return parseInt(Math.random() * (99999 - 10000) + 10000);
  };

  //Manejador de enviar datos
  const handleSubmit = (e) => {
    e.preventDefault();
    const newTask = {
      ...task,
      id: handleRandom(),
    };

    saveTask(newTask);
    setTask(initialState);
  };

  return (
    <form onSubmit={handleSubmit} className="card">
      <div className="card-header text-center">
        <h4>Add a task</h4>
      </div>

      <div className="card-body">
        <div className="form-group">
          <input
            type="text"
            className="form-control"
            placeholder="Title"
            onChange={(e) => handleChange("title", e.target.value)}
            value={task.title}
          />
        </div>

        <div className="form-group">
          <input
            type="text"
            className="form-control"
            placeholder="Description"
            onChange={(e) => handleChange("description", e.target.value)}
            value={task.description}
          />
        </div>

        <button type="submit" className="btn btn-primary btn-block">
          Save
        </button>
      </div>
    </form>
  );
};

export default HomeForm;

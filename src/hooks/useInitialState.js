import { useState } from "react";

const initialState = {
  tasks: [],
  task: {
    id: 0,
    title: "",
    description: "",
  },
};

const useInitialState = () => {
  const [state, setState] = useState(initialState);

  //Guardamos una tarea
  const saveTask = (payload) => {
    setState({
      ...state,
      tasks: [...state.tasks, payload],
    });
  };

  //Eliminamos una tarea
  const deleteTask = (payload) => {
    setState({
      ...state,
      tasks: state.tasks.filter((t) => t.id !== payload.id),
    });
  };

  return {
    state,
    saveTask,
    deleteTask,
  };
};

export default useInitialState;

import React from "react";

//Importing containers
import HomeList from "@containers/HomeList";
import HomeForm from "@containers/HomeForm";

const Home = () => {
  return (
    <div className="container my-4">
      <div className="row">
        <div className="col-lg-4 col-md-6 col-12 mx-auto">
          <HomeForm />
        </div>
      </div>

      <div className="row mt-5">
        <div className="table-responsive">
          <div className="col-12">
            <table className="table table-hover">
              <thead>
                <tr>
                  <th>ID</th>
                  <th>Title</th>
                  <th>Description</th>
                  <th>Actions</th>
                </tr>
              </thead>
              <HomeList />
            </table>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Home;

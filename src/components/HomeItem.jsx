import React from "react";

const HomeItem = ({ t, d }) => {
  return (
    <tr>
      <td>{t.id}</td>
      <td>{t.title}</td>
      <td>{t.description}</td>
      <td>
        <button className="btn btn-danger" onClick={() => d(t)}>
          Delete
        </button>
      </td>
    </tr>
  );
};

export default HomeItem;
